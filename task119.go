package main

import "fmt"

func main() {
	fmt.Println(getRow(3))
}

func getRow(rowIndex int) []int {
	list := tgenerate(rowIndex + 1)

	return list[rowIndex]
}

func tgenerate(numRows int) [][]int {
	list := make([][]int, numRows)

	for i := 0; i < numRows; i++ {
		list[i] = make([]int, i+1)
	}

	for i := 0; i < numRows; i++ {
		list[i][0] = 1
		list[i][len(list[i])-1] = 1
	}

	for i := 0; i < numRows; i++ {
		for j := 0; j < len(list[i]); j++ {
			if list[i][j] == 0 {
				list[i][j] = list[i-1][j-1] + list[i-1][j]
			}
		}
	}

	return list
}
