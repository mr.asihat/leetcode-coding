package main

import "fmt"

func main() {
	fmt.Println(generate(5))
}

func generate(numRows int) [][]int {
	list := make([][]int, numRows)

	for i := 0; i < numRows; i++ {
		list[i] = make([]int, i+1)
	}

	for i := 0; i < numRows; i++ {
		list[i][0] = 1
		list[i][len(list[i])-1] = 1
	}

	for i := 0; i < numRows; i++ {
		for j := 0; j < len(list[i]); j++ {
			if list[i][j] == 0 {
				list[i][j] = list[i-1][j-1] + list[i-1][j]
			}
		}
	}

	return list
}
