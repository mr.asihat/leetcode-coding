package main

import "fmt"

func main() {
	a := 1
	showtext(&a)
	fmt.Println(a)
}

func showtext(a *int) {
	*a = 2
	&a = 2123
}

type TreeNode struct {
	Val   int
	Left  *TreeNode
	Right *TreeNode
}

func hasPathSum(root *TreeNode, targetSum int) bool {
	if root == nil {
		return false
	}
	currSum := root.Val
	if currSum == targetSum && root.Left == nil && root.Right == nil {
		return true
	}

	result := false

	pathSum(root.Left, targetSum, currSum, &result)
	pathSum(root.Right, targetSum, currSum, &result)

	return result
}

func pathSum(root *TreeNode, targetSum int, currSum int, result *bool) {
	if root == nil {
		return
	}

	currSum += root.Val
	if currSum == targetSum && root.Left == nil && root.Right == nil {
		*result = true
		return
	}
	pathSum(root.Left, targetSum, currSum, result)
	pathSum(root.Right, targetSum, currSum, result)
}
